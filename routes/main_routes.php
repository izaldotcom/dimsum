<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware'=>['auth']], function () {        
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['prefix' => 'utilitas', 'middleware'=>['auth', 'role:admin']], function () {
	Route::resource('/user', 'Utilitas\UserController')->except(['show']);

    Route::get('/user/roles/{id}', 'Utilitas\UserController@roles')->name('user.roles');
    Route::put('/user/roles/{id}', 'Utilitas\UserController@setRole')->name('user.set_role');
    Route::post('/user/permission', 'Utilitas\UserController@addPermission')->name('user.add_permission');
    Route::get('/user/role-permission', 'Utilitas\UserController@rolePermission')->name('user.roles_permission');
    Route::put('/user/permission/{role}', 'Utilitas\UserController@setRolePermission')->name('user.setRolePermission');

    Route::resource('/role', 'Utilitas\RoleController');

	Route::resource('/module', 'Utilitas\ModuleController');
	Route::resource('/menu', 'Utilitas\MenuController');
});

Route::group(['prefix' => 'master', 'middleware'=>['auth']], function () {
	Route::resource('/tim', 'Master\TimController');
    Route::resource('/kategori', 'Master\KategoriController');
    Route::resource('/produk', 'Master\ProdukController');
    Route::resource('/supplier', 'Master\SupplierController');
	Route::resource('/pemain', 'Master\PemainController');
});

Route::group(['prefix' => 'transaksi', 'middleware'=>['auth']], function () {
    Route::resource('/pertandingan', 'Transaksi\PertandinganController');
});