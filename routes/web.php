<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Illuminate\Auth\Middleware\Authenticate
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    return redirect(route('login'));
});

Auth::routes();
