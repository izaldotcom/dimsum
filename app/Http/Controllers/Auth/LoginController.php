<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Module;
use App\Menu;
use App\User;
use App\Pegawai;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $user = User::where("username", $request->username)->where("status", "aktif")->first();

        $modul = Module::orderBy('no_order', 'ASC')->get();
        $menu = Menu::with('modul')->where('status', 1)->orderBy('no_order', 'ASC')->get();

        $query = "
                SELECT b.module_name FROM 
                role_has_permissions a
                JOIN permissions b ON a.permission_id = b.id
                LEFT JOIN roles c ON a.role_id = c.id
                LEFT JOIN model_has_roles d ON c.id = d.role_id
                LEFT JOIN mst_user e ON e.id = d.model_id
                WHERE e.username = '".$user->username."'
                GROUP BY b.module_name
                ";
        $userMenu = DB::select(DB::raw($query));

        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        if (auth()->attempt(['username' => $request->username, 'password' => $request->password, 'status' => 'aktif']))
        {
            Session::put('sess_module', $modul);
            Session::put('sess_menu', $menu);
            Session::put('sess_usermenu', $userMenu);
            return redirect()->intended('/home');
        }
        self::danger('Username atau password yang anda masukkan salah.');
        return redirect()->back();
    }
}
