<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class SupplierController extends Controller
{
    public function index()
	{
	    $supplier = Supplier::where('is_delete', 0)->orderBy('add_date', 'DESC')->paginate(10);
    	return view('master.supplier.index', compact('supplier'));
	}

	public function create()
	{
	    return view('master.supplier.create');
	}

	public function store(Request $request)
    {	        
	    $this->validate($request, [
	        'nama' => 'required|string|max:255|unique:mst_supplier',
	        'kode' => 'required|string|max:255|unique:mst_supplier',
	        'alamat' => 'nullable|string',
	        'no_telp' => 'nullable|string',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	    	
	        $supplier = Supplier::create([
	            'nama' => $request->nama,
	            'kode' => $request->kode,
	            'alamat' => $request->alamat,
	            'no_telp' => $request->no_telp,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);
	        
	        self::success('Data supplier berhasil ditambahkan.');
	        return redirect(route('supplier.index'));
	    } catch (\Exception $e) {
	        self::danger('Data supplier gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $supplier = Supplier::findOrFail($id);
	    $supplier->update([
        	'is_delete' => 1
        ]);

	    self::danger('Data supplier berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $supplier = Supplier::findOrFail($id);
	    return view('master.supplier.edit', compact('supplier'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'nama' => 'required|string|max:255',
	        'kode' => 'required|string|max:255',
	        'alamat' => 'nullable|string',
	        'no_telp' => 'nullable|string',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	    	$supplier = Supplier::findOrFail($id);

	        $supplier->update([
	        	'nama' => $request->nama,
	        	'kode' => $request->kode,
	            'alamat' => $request->alamat,
	            'no_telp' => $request->no_telp,
	        	'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data supplier berhasil diubah.');
			return redirect(route('supplier.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data tim gagal diubah.');
	        return redirect()->back();
	    }
    }
}
