<?php

namespace App\Http\Controllers\Master;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tim;
use App\Pemain;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class PemainController extends Controller
{
    public function index()
	{
    	$pemain = Pemain::with('tim')->where('is_delete', 0)->orderBy('add_date', 'DESC')->paginate(10);
		return view('master.pemain.index', compact('pemain'));
	}

	public function create()
	{	
	    $tim = Tim::orderBy('nama', 'ASC')->get();
	    return view('master.pemain.create', compact('tim'));
	}

	public function saveFile($name, $foto)
    {        
	    $images = str_slug($name) . time() . '.' . $foto->getClientOriginalExtension();
	    $path = public_path('uploads/pemain');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($foto)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'nama' => 'required|string|max:255',
	        'jenis_kelamin' => 'required|integer',
	        'tinggi_badan' => 'required|string|max:100',
	        'berat_badan' => 'required|string|max:100',
	        'posisi' => 'required|string|max:100',
	        'no_punggung' => 'required|string|unique:mst_pemain',
	        'tempat_lahir' => 'required|string|max:100',
	        'tgl_lahir' => 'required|date',
	        'alamat' => 'required|string',
	        'no_telp' => 'required|string|max:100',
	        'id_tim' => 'required|integer',
	        'foto' => 'nullable|image|mimes:jpg,png,jpeg',
	        'add_date' => date("Y-m-d H:i:s"),
        	'add_user' => Auth::user()->name 
	    ]);	

	    try {
	        $foto = null;
	        if ($request->hasFile('foto')) {
	            $foto = $this->saveFile($request->name, $request->file('foto'));
	        }

	        $pemain = Pemain::create([
	            'nama' => $request->nama,
	            'jenis_kelamin' => $request->jenis_kelamin,
	            'tinggi_badan' => $request->tinggi_badan,
	            'berat_badan' => $request->berat_badan,
	            'posisi' => $request->posisi,
	            'no_punggung' => $request->no_punggung,
	            'tempat_lahir' => $request->tempat_lahir,
	            'tgl_lahir' => $request->tgl_lahir,
	            'alamat' => $request->alamat,
	            'no_telp' => $request->no_telp,
	            'id_tim' => $request->id_tim,
	            'foto' => $foto,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);

	        self::success('Data pemain berhasil ditambahkan.');
	        return redirect(route('pemain.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data pemain gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $pemain = Pemain::findOrFail($id);
		$pemain->update([
        	'is_delete' => 1
        ]);	

	    self::danger('Data pemain berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $pemain = Pemain::findOrFail($id);
	    $tim = Tim::orderBy('nama', 'ASC')->get();
	    $profil = Pemain::with('tim')->orderBy('add_date', 'DESC')->where('id', $id)->first();
	    return view('master.pemain.edit', compact('pemain', 'tim', 'profil'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'nama' => 'required|string|max:255',
	        'jenis_kelamin' => 'required|integer',
	        'tinggi_badan' => 'required|string|max:100',
	        'berat_badan' => 'required|string|max:100',
	        'posisi' => 'required|string|max:100',
	        'no_punggung' => 'required|string|max:100',
	        'tempat_lahir' => 'required|string|max:100',
	        'tgl_lahir' => 'required|date',
	        'alamat' => 'required|string',
	        'no_telp' => 'required|string|max:100',
	        'id_tim' => 'required|integer',
	        'foto' => 'nullable|image|mimes:jpg,png,jpeg',
	        'modified_date' => date("Y-m-d H:i:s"),
        	'modified_user' => Auth::user()->name 
	    ]);

	    try {
	        $pemain = Pemain::findOrFail($id);
	        $foto = $pemain->foto;

	        if ($request->hasFile('foto')) {
	            !empty($foto) ? File::delete(public_path('uploads/pemain/' . $foto)):null;
	            $foto = $this->saveFile($request->name, $request->file('foto'));
	        }
	        
	        $pemain->update([
	        	'nama' => $request->nama,
	            'jenis_kelamin' => $request->jenis_kelamin,
	            'tinggi_badan' => $request->tinggi_badan,
	            'berat_badan' => $request->berat_badan,
	            'posisi' => $request->posisi,
	            'no_punggung' => $request->no_punggung,
	            'tempat_lahir' => $request->tempat_lahir,
	            'tgl_lahir' => $request->tgl_lahir,
	            'alamat' => $request->alamat,
	            'no_telp' => $request->no_telp,
	            'id_tim' => $request->id_tim,
	            'foto' => $foto,
	            'modified_date' => date("Y-m-d H:i:s"),
	            'modified_user' => strtolower(Auth::user()->name)
	        ]);

	        self::success('Data pemain berhasil diubah.');
			return redirect(route('pemain.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data pemain gagal diubah.');
	        return redirect()->back();
	    }
    }
}
