<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tim;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class TimController extends Controller
{
    public function index()
	{
	    $tim = Tim::where('is_delete', 0)->orderBy('add_date', 'DESC')->paginate(10);
    	return view('master.tim.index', compact('tim'));
	}

	public function create()
	{
	    return view('master.tim.create');
	}

	public function saveFile($name, $logo)
    {        
	    $images = str_slug($name) . time() . '.' . $logo->getClientOriginalExtension();
	    $path = public_path('uploads/tim');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($logo)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
	    $this->validate($request, [
	        'nama' => 'required|string|max:255|unique:mst_tim',
	        'logo' => 'nullable|image|mimes:jpg,png,jpeg',
	        'tahun_berdiri' => 'required|string|max:100',
	        'alamat' => 'required|string',
	        'kota' => 'required|string|max:100',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	    	$logo = null;
	        if ($request->hasFile('logo')) {
	            $logo = $this->saveFile($request->name, $request->file('logo'));
	        }
	        
	        $tim = Tim::create([
	            'nama' => $request->nama,
	            'logo' => $logo,
	            'tahun_berdiri' => $request->tahun_berdiri,
	            'alamat' => $request->alamat,
	            'kota' => $request->kota,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);
	        
	        self::success('Data tim berhasil ditambahkan.');
	        return redirect(route('tim.index'));
	    } catch (\Exception $e) {
	        self::danger('Data tim gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $tim = Tim::findOrFail($id);
	    $tim->update([
        	'is_delete' => 1
        ]);

	    self::danger('Data tim berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $tim = Tim::findOrFail($id);
	    return view('master.tim.edit', compact('tim'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'nama' => 'required|string|max:255',
	        'logo' => 'nullable|string|mimes:jpg,png,jpeg',
	        'tahun_berdiri' => 'required|string|max:100',
	        'alamat' => 'required|string',
	        'kota' => 'required|string|max:100',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	    	$tim = Tim::findOrFail($id);
	        $logo = $tim->logo;

	        if ($request->hasFile('logo')) {
	            !empty($logo) ? File::delete(public_path('uploads/tim/' . $logo)):null;
	            $logo = $this->saveFile($request->name, $request->file('logo'));
	        }

	        $tim = Tim::findOrFail($id);

	        $tim->update([
	        	'nama' => $request->nama,
	            'logo' => $logo,
	            'tahun_berdiri' => $request->tahun_berdiri,
	            'alamat' => $request->alamat,
	            'kota' => $request->kota,
	        	'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data tim berhasil diubah.');
			return redirect(route('tim.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data tim gagal diubah.');
	        return redirect()->back();
	    }
    }
}
