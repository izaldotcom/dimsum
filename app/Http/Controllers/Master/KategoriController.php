<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kategori;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class KategoriController extends Controller
{
    public function index()
	{
	    $kategori = Kategori::where('is_delete', 0)->orderBy('add_date', 'DESC')->paginate(10);
    	return view('master.kategori.index', compact('kategori'));
	}

	public function create()
	{
	    return view('master.kategori.create');
	}

	public function store(Request $request)
    {	        
	    $this->validate($request, [
	        'nama' => 'required|string|max:255|unique:mst_kategori',
	        'deskripsi' => 'nullable|string',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	    	
	        $kategori = Kategori::create([
	            'nama' => $request->nama,
	            'deskripsi' => $request->deskripsi,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);
	        
	        self::success('Data kategori berhasil ditambahkan.');
	        return redirect(route('kategori.index'));
	    } catch (\Exception $e) {
	        self::danger('Data kategori gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $kategori = Kategori::findOrFail($id);
	    $kategori->update([
        	'is_delete' => 1
        ]);

	    self::danger('Data kategori berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $kategori = Kategori::findOrFail($id);
	    return view('master.kategori.edit', compact('kategori'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'nama' => 'required|string|max:255',
	        'deskripsi' => 'nullable|string',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	    	$kategori = Kategori::findOrFail($id);

	        $kategori->update([
	        	'nama' => $request->nama,
	            'deskripsi' => $request->deskripsi,
	        	'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data kategori berhasil diubah.');
			return redirect(route('kategori.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data tim gagal diubah.');
	        return redirect()->back();
	    }
    }
}
