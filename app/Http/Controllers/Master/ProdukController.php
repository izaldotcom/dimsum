<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produk;
use App\Kategori;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class ProdukController extends Controller
{
    public function index()
	{
	    $produk = Produk::where('is_delete', 0)->with('kategori')->orderBy('add_date', 'DESC')->paginate(10);
    	return view('master.produk.index', compact('produk'));
	}

	public function create()
	{
		$kategori = Kategori::orderBy('nama', 'ASC')->get();
	    return view('master.produk.create', compact('kategori'));
	}

	public function saveFile($name, $gambar_produk)
    {        
	    $images = str_slug($name) . time() . '.' . $gambar_produk->getClientOriginalExtension();
	    $path = public_path('uploads/produk');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($gambar_produk)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
	    $this->validate($request, [
	        'kategori_id' => 'required|integer',
	        'nama' => 'required|string|max:255|unique:mst_produk',
	        'deskripsi' => 'nullable|string',
	        'kode_produk' => 'required|string|max:255|unique:mst_produk',
	        'harga' => 'required|integer',
	        'barcode' => 'required|string|max:255|unique:mst_produk',
	        'satuan' => 'required|string',
	        'gambar_produk' => 'nullable|image|mimes:jpg,png,jpeg',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	    	$gambar_produk = null;
	        if ($request->hasFile('gambar_produk')) {
	            $gambar_produk = $this->saveFile($request->nama, $request->file('gambar_produk'));
	        }
	        
	        $produk = Produk::create([
	            'kategori_id' => $request->kategori_id,
	            'nama' => $request->nama,
	            'deskripsi' => $request->deskripsi,
	            'kode_produk' => $request->kode_produk,
	            'harga' => $request->harga,
	            'barcode' => $request->barcode,
	            'satuan' => $request->satuan,
	            'gambar_produk' => $gambar_produk,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);
	        
	        self::success('Data produk berhasil ditambahkan.');
	        return redirect(route('produk.index'));
	    } catch (\Exception $e) {
	        self::danger('Data produk gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $produk = Produk::findOrFail($id);
	    $produk->update([
        	'is_delete' => 1
        ]);

	    self::danger('Data produk berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $produk = Produk::findOrFail($id);
	    $kategori = Kategori::orderBy('nama', 'ASC')->get();
	    return view('master.produk.edit', compact('produk', 'kategori'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'kategori_id' => 'required|integer',
	        'nama' => 'required|string|max:255',
	        'deskripsi' => 'nullable|string',
	        'kode_produk' => 'required|string|max:255',
	        'harga' => 'required|integer',
	        'barcode' => 'required|string|max:255',
	        'satuan' => 'required|string',
	        'gambar_produk' => 'nullable|image|mimes:jpg,png,jpeg',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	    	$produk = Produk::findOrFail($id);
	        $gambar_produk = $produk->gambar_produk;

	        if ($request->hasFile('gambar_produk')) {
	            !empty($gambar_produk) ? File::delete(public_path('uploads/produk/' . $gambar_produk)):null;
	            $gambar_produk = $this->saveFile($request->nama, $request->file('gambar_produk'));
	        }

	        $produk->update([
	        	'kategori_id' => $request->kategori_id,
	            'nama' => $request->nama,
	            'deskripsi' => $request->deskripsi,
	            'kode_produk' => $request->kode_produk,
	            'harga' => $request->harga,
	            'barcode' => $request->barcode,
	            'satuan' => $request->satuan,
	            'gambar_produk' => $gambar_produk,
	        	'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data produk berhasil diubah.');
			return redirect(route('produk.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data produk gagal diubah.');
	        return redirect()->back();
	    }
    }
}
