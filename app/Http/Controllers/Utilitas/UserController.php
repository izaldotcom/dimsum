<?php

namespace App\Http\Controllers\Utilitas;

use Auth;
// use Illuminate\Http\Request;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\User;
use App\Menu;
use Spatie\Permission\Models\Permission;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {   
        $dataUser = User::orderBy('created_at', 'DESC')->paginate(10);
        return view('utilitas.user.index', compact('dataUser'));
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('utilitas.user.create', compact('role'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'username' => 'required|string|unique:mst_user',
            'password' => 'required|min:6',
            'role' => 'required|string|exists:roles,name'
        ]);

        $user = User::firstOrCreate([
            'username' => $request->username
        ], [
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'status' => 'suspend'
        ]);

        $user->assignRole($request->role);

        self::success('Data user berhasil ditambahkan.');
        return redirect(route('user.index'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('utilitas.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'username' => 'required|string|exists:mst_user,username',
            'password' => 'nullable|min:6',
            'status' => 'required|string',
        ]);

        $user = User::findOrFail($id);
        $password = !empty($request->password) ? bcrypt($request->password):$user->password;
        $user->update([
            'name' => $request->name,
            'password' => $password,
            'status' => $request->status
        ]);
        self::success('Data user berhasil diubah.');
        return redirect(route('user.index'));
    }

    public function destroy($id)
    {
        $user = User::where('id', $id)->delete();
        self::danger('Data user berhasil dihapus.');
        return redirect()->back();
    }

    public function roles(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');
        return view('utilitas.user.roles', compact('user', 'roles'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);
        
        $user = User::findOrFail($id);
        $user->syncRoles($request->role);

        self::success('Data role berhasil diset.');
        return redirect()->back();
    }

    public function rolePermission(Request $request)
    {
        $menu = Menu::with('modul')->orderBy('id_module', 'ASC')->orderBy('no_order', 'ASC')->get();
        $role = $request->get('role');

        $permissions = null;
        $hasPermission = null;
        
        $roles = Role::all()->pluck('name');
        
        if (!empty($role)) {
            $getRole = Role::findByName($role);
            
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            
            $permissions = Permission::all()->pluck('name');
        }
        return view('utilitas.user.role_permission', compact('roles', 'permissions', 'hasPermission', 'menu'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $explodeModul = explode('-', $request->name);

        $permission = Permission::firstOrCreate([
            'name' => $request->name,
            'module_name' => $explodeModul[0]
        ]);
        self::success('Data permission berhasil ditambahkan.');
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::findByName($role);
        $role->syncPermissions($request->permission);
        self::success('Data permission berhasil disimpan.');
        return redirect()->back();
    }

}