<?php

namespace App\Http\Controllers\Utilitas;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Module;

class menuController extends Controller
{

    public function index()
	{
	    $menu = Menu::with('modul')->orderBy('id_module', 'ASC')->orderBy('no_order', 'ASC')->paginate(10);
    	return view('utilitas.menu.index', compact('menu'));
	}

	public function create()
	{
		$modul = Module::orderBy('name', 'ASC')->get();
	    return view('utilitas.menu.create', compact('modul'));
	}

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	    	'id_module' => 'required|integer',
	        'name' => 'string|max:100',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'status' => 'integer',
	        'no_order' => 'integer',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	        
	        $menu = Menu::create([
	            'id_module' => $request->id_module,
	            'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'status' => $request->status,
	            'no_order' => $request->no_order,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);
	        
	        self::success('Data menu berhasil ditambahkan.');
	        return redirect(route('menu.index'));
	    } catch (\Exception $e) {
	        self::danger('Data menu gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $menu = Menu::findOrFail($id);
	    $menu->delete();

	    self::danger('Data menu berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$modul = Module::orderBy('name', 'ASC')->get();
	    $menu = Menu::findOrFail($id);
	    return view('utilitas.menu.edit', compact('menu', 'modul'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'id_module' => 'required|integer',
	        'name' => 'string|max:100',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'status' => 'integer',
	        'no_order' => 'integer',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	        $menu = Menu::findOrFail($id);

	        $menu->update([
	        	'id_module' => $request->id_module,
	            'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'status' => $request->status,
	            'no_order' => $request->no_order,
	            'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data menu berhasil diubah.');
			return redirect(route('menu.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data menu gagal diubah.');
	        return redirect()->back();
	    }
    }
}
