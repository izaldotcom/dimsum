<?php

namespace App\Http\Controllers\Utilitas;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module;
use App\Menu;
use App\Permission;

class ModuleController extends Controller
{

    public function index()
	{
	    $module = Module::orderBy('no_order', 'ASC')->paginate(10);
    	return view('utilitas.module.index', compact('module'));
	}

	public function create()
	{
	    return view('utilitas.module.create');
	}

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'name' => 'string|max:100|unique:mst_module',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'is_parent' => 'integer',
	        'no_order' => 'integer|unique:mst_module',
	        'add_date' => date("Y-m-d H:i:s"),
	        'add_user' => Auth::user()->name 
	    ]);	

	    try {
	        
	        $module = Module::create([
	            'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order,
	            'add_user' => strtolower(Auth::user()->name)
	        ]);

	        if($request->is_parent == 0){
		        $menu = Menu::create([
		            'id_module' => $module->id,
		            'status' => 0,
		            'no_order' => 0,
		            'add_user' => strtolower(Auth::user()->name)
		        ]);
	        }
	        
	        self::success('Data module berhasil ditambahkan.');
	        return redirect(route('module.index'));
	    } catch (\Exception $e) {
	        self::danger('Data module gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $module = Module::findOrFail($id);
	    $menu = Menu::where('id_module', $module->id)->delete();
	    $permission = Permission::where('module_name', str_replace(' ', '', strtolower($module->name)))->delete();
	    $module->delete();

	    self::danger('Data module berhasil dihapus.');
	    return redirect()->back();
    }

    public function edit($id)
	{
	    $module = Module::findOrFail($id);
	    return view('utilitas.module.edit', compact('module'));
	}

	public function update(Request $request, $id)
    {  
       	//validasi
	    $this->validate($request, [
	        'name' => 'string|max:100',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'is_parent' => 'integer',
	        'no_order' => 'no_order',
	        'modified_date' => date("Y-m-d H:i:s"),
	        'modified_user' => Auth::user()->name
	    ]);

	    try {
	        $module = Module::findOrFail($id);

	        $module->update([
	        	'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order,
	            'modified_date' => date("Y-m-d H:i:s"),
	        	'modified_user' => strtolower(Auth::user()->name)
	        ]);
	        
		    self::success('Data module berhasil diubah.');
			return redirect(route('module.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data module gagal diubah.');
	        return redirect()->back();
	    }
    }
}
