<?php

namespace App\Http\Controllers\Transaksi;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tim;
use App\Pemain;
use App\Pertandingan;

class PertandinganController extends Controller
{
    public function index()
	{
    	$pertandingan = Pertandingan::with('home', 'away')->where('is_delete', 0)->orderBy('add_date', 'DESC')->paginate(10);
		return view('transaksi.pertandingan.index', compact('pertandingan'));
	}

	public function create()
	{	
	    $tim = Tim::orderBy('nama', 'ASC')->get();
	    return view('transaksi.pertandingan.create', compact('tim'));
	}

	public function store(Request $request)
    {	        
	    $this->validate($request, [
	        'tgl_pertandingan' => 'required|date',
	        'waktu_pertandingan' => 'required|string|max:100',
	        'home_team' => 'required|integer',
	        'away_team' => 'required|integer',
	        'skor_home' => 'required|integer',
	        'skor_away' => 'required|integer',
	        'detail_gol_home' => 'nullable|string',
	        'detail_gol_away' => 'nullable|string',
	        'add_date' => date("Y-m-d H:i:s"),
        	'add_user' => Auth::user()->name 
	    ]);	

	    try {

			$detail_gol_home = [];
			$detail_gol_away = [];

			for ($home=1; $home <= $request->skor_home; $home++) { 
	            $menit_gol_home = "menit_gol_home-".$home;
	            $pencetak_gol_home = "pencetak_gol_home-".$home;

			    $detail_gol_home[] = [
			        'home_team' => $request->home_team,
			        'menit_gol_home' => $request->$menit_gol_home,
			        'pencetak_gol_home' => $request->$pencetak_gol_home
			    ];
			}
			
			for ($away=1; $away <= $request->skor_away; $away++) {
				$menit_gol_away = "menit_gol_away-".$away;
	            $pencetak_gol_away = "pencetak_gol_away-".$away;

			    $detail_gol_away[] = [
			        'away_team' => $request->away_team,
			        'menit_gol_away' => $request->$menit_gol_away,
			        'pencetak_gol_away' => $request->$pencetak_gol_away
			    ];
			}

	        $pertandingan = Pertandingan::create([
	            'tgl_pertandingan' => $request->tgl_pertandingan,
	            'waktu_pertandingan' => $request->waktu_pertandingan,
	            'home_team' => $request->home_team,
	            'away_team' => $request->away_team,
	            'skor_home' => $request->skor_home,
	            'skor_away' => $request->skor_away,
				'detail_gol_home' => json_encode($detail_gol_home),
				'detail_gol_away' => json_encode($detail_gol_away),
	            'add_user' => strtolower(Auth::user()->name)
	        ]);

	        self::success('Data pertandingan berhasil ditambahkan.');
	        return redirect(route('pertandingan.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data pertandingan gagal ditambahkan.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
	    $pertandingan = Pertandingan::findOrFail($id);
		$pertandingan->update([
        	'is_delete' => 1
        ]);	

	    self::danger('Data pertandingan berhasil dihapus.');
	    return redirect()->back();
    }

}
