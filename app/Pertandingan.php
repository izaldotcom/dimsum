<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertandingan extends Model
{
	protected $table = 'trans_pertandingan';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];


   	public function home()
	{	
	    return $this->belongsTo("App\Tim", "home_team");
	}

	public function away()
	{	
	    return $this->belongsTo("App\Tim", "away_team");
	}

}
