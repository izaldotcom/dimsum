<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
	protected $table = 'mst_kategori';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

}