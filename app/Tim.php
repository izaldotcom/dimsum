<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tim extends Model
{
	protected $table = 'mst_tim';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

}