<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemain extends Model
{
	protected $table = 'mst_pemain';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];


   	public function tim()
	{	
	    return $this->belongsTo("App\Tim", "id_tim");
	}

}
