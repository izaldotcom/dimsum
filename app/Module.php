<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $table = 'mst_module';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

 //    public function menu()
	// {	
	//     return $this->hasMany("App\Menu", "id_module");
	// }
}
