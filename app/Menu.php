<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table = 'mst_menu';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

    public function modul()
	{	
	    return $this->belongsTo("App\Module", "id_module");
	}
}
