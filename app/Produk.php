<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
	protected $table = 'mst_produk';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

    public function kategori()
	{	
	    return $this->belongsTo("App\Kategori", "kategori_id");
	}
}