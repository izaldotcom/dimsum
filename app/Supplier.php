<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
	protected $table = 'mst_supplier';
	protected $primaryKey = 'id';
	public $timestamps = false;
    protected $guarded = [];

}