  @extends('layouts.master')

  @section('title')
      <title>Edit Kategori</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Kategori</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('kategori.index') }}">Kategori</a></li>
              <li class="breadcrumb-item active">Edit Kategori</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('kategori.update', $kategori->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" 
                            value="{{ $kategori->nama }}"
                            class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('nama') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" 
                            cols="5" rows="5" 
                            class="form-control {{ $errors->has('deskripsi') ? 'is-invalid':'' }}">{{ $kategori->deskripsi }}</textarea>
                        <p class="text-danger">{{ $errors->first('deskripsi') }}</p>
                    </div>
                    <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection