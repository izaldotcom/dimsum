  @extends('layouts.master')

  @section('title')
      <title>Edit Pemain</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Pemain</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('pemain.index') }}">Pemain</a></li>
              <li class="breadcrumb-item active">Edit Pemain</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('pemain.update', $pemain->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Tim</label>
                        <select name="id_tim" id="id_tim" 
                            required class="form-control {{ $errors->has('id_tim') ? 'is-invalid':'' }}">
                            <option value="">Pilih</option>
                            @foreach ($tim as $row)
                                <option value="{{ $row->id }}" {{ $row->id == $pemain->id_tim ? 'selected':'' }}>
                                    {{ ucfirst($row->nama) }}
                                </option>
                            @endforeach
                        </select>
                        <p class="text-danger">{{ $errors->first('id_tim') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" value="{{ $pemain->nama }}" required>
                        <p class="text-danger">{{ $errors->first('nama') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" 
                            required class="form-control {{ $errors->has('jenis_kelamin') ? 'is-invalid':'' }}">
                            <option value="1" {{ $pemain->jenis_kelamin == '1' ? 'selected':'' }}>Laki-laki</option>
                            <option value="2" {{ $pemain->jenis_kelamin == '2' ? 'selected':'' }}>Perempuan</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('jenis_kelamin') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Tinggi Badan (cm)</label>
                        <input type="text" name="tinggi_badan" class="form-control {{ $errors->has('tinggi_badan') ? 'is-invalid':'' }}" value="{{ $pemain->tinggi_badan }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                        <p class="text-danger">{{ $errors->first('tinggi_badan') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Berat Badan (kg)</label>
                        <input type="text" name="berat_badan" class="form-control {{ $errors->has('berat_badan') ? 'is-invalid':'' }}" value="{{ $pemain->berat_badan }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                        <p class="text-danger">{{ $errors->first('berat_badan') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Posisi</label>
                        <select name="posisi" id="posisi" 
                            required class="form-control {{ $errors->has('posisi') ? 'is-invalid':'' }}">
                            <option value="1" {{ $pemain->posisi == '1' ? 'selected':'' }}>Penyerang</option>
                            <option value="2" {{ $pemain->posisi == '2' ? 'selected':'' }}>Gelandang</option>
                            <option value="3" {{ $pemain->posisi == '3' ? 'selected':'' }}>Bertahan</option>
                            <option value="4" {{ $pemain->posisi == '4' ? 'selected':'' }}>Penjaga Gawang</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('posisi') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">No Punggung</label>
                        <input type="text" name="no_punggung" class="form-control {{ $errors->has('no_punggung') ? 'is-invalid':'' }}" value="{{ $pemain->no_punggung }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                        <p class="text-danger">{{ $errors->first('no_punggung') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control {{ $errors->has('tempat_lahir') ? 'is-invalid':'' }}" value="{{ $pemain->tempat_lahir }}" required>
                        <p class="text-danger">{{ $errors->first('tempat_lahir') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input type="text" name="tgl_lahir" 
                            class="form-control {{ $errors->has('tgl_lahir') ? 'is-invalid':'' }}"
                            id="tanggal_lahir" value="{{ $pemain->tgl_lahir }}"
                            >
                        <p class="text-danger">{{ $errors->first('tgl_lahir') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="alamat" id="alamat" 
                            cols="5" rows="5" 
                            class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}">{{ $pemain->alamat }}</textarea>
                        <p class="text-danger">{{ $errors->first('alamat') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Nomor Telepon</label>
                        <input type="text" name="no_telp" class="form-control {{ $errors->has('no_telp') ? 'is-invalid':'' }}" value="{{ $pemain->no_telp }}" required>
                        <p class="text-danger">{{ $errors->first('no_telp') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Foto</label>
                        <input type="file" name="foto" class="form-control">
                        <p class="text-danger">{{ $errors->first('foto') }}</p>
                        @if (!empty($pemain->foto))
                            <hr>
                            <img src="{{ asset('uploads/pemain/' . $pemain->foto) }}" 
                                alt="{{ $pemain->nama }}"
                                width="150px" height="150px">
                        @endif
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success btn-sm">
                            <i class="fa fa-send"></i> Simpan
                        </button>
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                    </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
@endsection