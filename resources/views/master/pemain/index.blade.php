  @extends('layouts.master')

  @section('title')
      <title>Data Pemain</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Pemain</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">Pemain</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                @if( auth()->user()->hasRole('admin'))
                <a href="{{ route('pemain.create') }}" class="btn btn-primary btn-sm">Tambah Baru</a>
                @endif
                  
                @include ('partials.messages')

                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Nama</td>
                                <td>Tim</td>
                                <td>Tempat, Tanggal Lahir</td>
                                <td>No Punggung</td>
                                <td>Posisi</td>
                                <td>Foto</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($pemain as $row)
                            <tr align="center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->tim->nama }}</td>
                                <td>{{ $row->tempat_lahir }}, {{ date_format(date_create($row->tgl_lahir), "d-m-Y") }}</td>
                                <td>{{ $row->no_punggung }}</td>
                                <td>
                                    @if($row->posisi==1)
                                      Penyerang
                                    @elseif($row->posisi==2)
                                      Gelandang
                                    @elseif($row->posisi==3)
                                      Bertahan
                                    @else
                                      Penjaga Gawang
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($row->foto))
                                        <img src="{{ asset('uploads/pemain/' . $row->foto) }}" 
                                            alt="{{ $row->nama }}" width="50px" height="50px">
                                    @else
                                        <img src="http://via.placeholder.com/50x50" alt="{{ $row->nama }}">
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('pemain.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <a href="{{ route('pemain.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                        @if( auth()->user()->hasRole('admin') )
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr align="center">
                                <td colspan="12" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection