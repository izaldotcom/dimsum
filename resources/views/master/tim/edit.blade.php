  @extends('layouts.master')

  @section('title')
      <title>Edit Tim</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Tim</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('tim.index') }}">Tim</a></li>
              <li class="breadcrumb-item active">Edit Tim</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('tim.update', $tim->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" 
                            value="{{ $tim->nama }}"
                            class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('nama') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Logo</label>
                        <input type="file" name="logo" class="form-control">
                        <p class="text-danger">{{ $errors->first('logo') }}</p>
                        @if (!empty($tim->logo))
                            <hr>
                            <img src="{{ asset('uploads/tim/' . $tim->logo) }}" 
                                alt="{{ $tim->nama }}"
                                width="150px" height="150px">
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="">Tahun Berdiri</label>
                        <input type="text" name="tahun_berdiri" class="form-control {{ $errors->has('tahun_berdiri') ? 'is-invalid':'' }}" value="{{ $tim->tahun_berdiri }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                        <p class="text-danger">{{ $errors->first('tahun_berdiri') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="alamat" id="alamat" 
                            cols="5" rows="5" 
                            class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}">{{ $tim->alamat }}</textarea>
                        <p class="text-danger">{{ $errors->first('alamat') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Kota</label>
                        <input type="text" name="kota" 
                            value="{{ $tim->kota }}"
                            class="form-control {{ $errors->has('kota') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('kota') }}</p>
                    </div>
                    <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection