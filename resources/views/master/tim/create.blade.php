  @extends('layouts.master')

  @section('title')
      <title>Tambah Tim</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Tim</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('tim.index') }}">Tim</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('tim.store') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                      <label for="">Nama</label>
                      <input type="text" name="nama" class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" required>
                      <p class="text-danger">{{ $errors->first('nama') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Logo</label>
                      <input type="file" name="logo" class="form-control">
                      <p class="text-danger">{{ $errors->first('logo') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Tahun Berdiri</label>
                      <input type="text" name="tahun_berdiri" class="form-control {{ $errors->has('tahun_berdiri') ? 'is-invalid':'' }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                      <p class="text-danger">{{ $errors->first('tahun_berdiri') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Alamat</label>
                      <textarea name="alamat" id="alamat" 
                          cols="5" rows="5" 
                          class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}" required></textarea>
                      <p class="text-danger">{{ $errors->first('alamat') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Kota</label>
                      <input type="text" name="kota" class="form-control {{ $errors->has('kota') ? 'is-invalid':'' }}" required>
                      <p class="text-danger">{{ $errors->first('kota') }}</p>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection