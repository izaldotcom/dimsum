  @extends('layouts.master')

  @section('title')
      <title>Edit Supplier</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Supplier</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('supplier.index') }}">Supplier</a></li>
              <li class="breadcrumb-item active">Edit Supplier</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('supplier.update', $supplier->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Kode Supplier</label>
                        <input type="text" name="kode" 
                            value="{{ $supplier->kode }}"
                            class="form-control {{ $errors->has('kode') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('kode') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Supplier</label>
                        <input type="text" name="nama" 
                            value="{{ $supplier->nama }}"
                            class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('nama') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="alamat" id="alamat" 
                            cols="5" rows="5" 
                            class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}">{{ $supplier->alamat }}</textarea>
                        <p class="text-danger">{{ $errors->first('alamat') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">No Telp</label>
                        <input type="text" name="no_telp" 
                            value="{{ $supplier->no_telp }}"
                            class="form-control {{ $errors->has('no_telp') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('no_telp') }}</p>
                    </div>
                    <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection