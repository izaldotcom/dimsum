  @extends('layouts.master')

  @section('title')
      <title>Tambah Produk</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Produk</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('produk.store') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                      <label for="">Kode Produk</label>
                      <input type="text" name="kode_produk" class="form-control {{ $errors->has('kode_produk') ? 'is-invalid':'' }}" required>
                      <p class="text-danger">{{ $errors->first('kode_produk') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Kategori</label>
                      <select name="kategori_id" id="kategori_id" 
                          required class="form-control {{ $errors->has('kategori_id') ? 'is-invalid':'' }}">
                          <option value="">Pilih</option>
                          @foreach ($kategori as $row)
                              <option value="{{ $row->id }}">{{ ucfirst($row->nama) }}</option>
                          @endforeach
                      </select>
                      <p class="text-danger">{{ $errors->first('kategori_id') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Barcode</label>
                      <input type="text" name="barcode" class="form-control {{ $errors->has('barcode') ? 'is-invalid':'' }}" required>
                      <p class="text-danger">{{ $errors->first('barcode') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Nama</label>
                      <input type="text" name="nama" class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" required>
                      <p class="text-danger">{{ $errors->first('nama') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Satuan</label>
                      <select name="satuan" id="satuan" 
                          required class="form-control {{ $errors->has('satuan') ? 'is-invalid':'' }}">
                          <option value="">Pilih</option>
                          <option value="Kg">Kg</option>
                          <option value="Pack">Pack</option>
                          <option value="Pcs">Pcs</option>
                          <option value="Sachet">Sachet</option>
                          <option value="Bal">Bal</option>
                          <option value="Sak">Sak</option>
                          <option value="Paket">Paket</option>
                          <option value="Dus">Dus</option>
                          <option value="Botol">Botol</option>
                          <option value="Galon">Galon</option>
                      </select>
                      <p class="text-danger">{{ $errors->first('satuan') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Harga</label>
                      <input type="text" name="harga" class="form-control {{ $errors->has('harga') ? 'is-invalid':'' }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                      <p class="text-danger">{{ $errors->first('harga') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Deskripsi</label>
                      <textarea name="deskripsi" id="deskripsi" 
                          cols="5" rows="5" 
                          class="form-control {{ $errors->has('deskripsi') ? 'is-invalid':'' }}"></textarea>
                      <p class="text-danger">{{ $errors->first('deskripsi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Gambar Produk</label>
                      <input type="file" name="gambar_produk" class="form-control">
                      <p class="text-danger">{{ $errors->first('gambar_produk') }}</p>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection