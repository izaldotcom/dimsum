  @extends('layouts.master')

  @section('title')
      <title>Data Menu</title>
  @endsection
  
  @section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Menu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">Menu</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                  <a href="{{ route('menu.create') }}" class="btn btn-primary btn-sm">Tambah Baru</a>
                  
                  @include ('partials.messages')

                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Modul</td>
                                <td>Nama</td>
                                <td>Ikon</td>
                                <td>Url</td>
                                <td>Deksripsi</td>
                                <td>Urutan Menu</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($menu as $row)
                            <tr align="center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->modul->name }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->icon }}</td>
                                <td>{{ $row->url }}</td>
                                <td>{{ $row->description }}</td>
                                <td>{{ $row->no_order }}</td>
                                <td>
                                    @if ($row->status == 0)
                                    <label class="badge badge-danger">Tidak Aktif</label>
                                    @else
                                    <label class="badge badge-success">Aktif</label>
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('menu.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        @if(!empty($row->name) && !$row->no_order == 0)
                                        <a href="{{ route('menu.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                        @else
                                        <a href="#" class="btn btn-warning btn-sm disabled"><i class="fa fa-edit"></i></a>
                                        @endif
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr align="center">
                                <td colspan="12" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection