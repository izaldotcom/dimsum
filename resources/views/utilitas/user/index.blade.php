 @extends('layouts.master')

  @section('title')
      <title>Manajemen User</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manajemen User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                  <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm">Tambah Baru</a>
                  
                  @include ('partials.messages')

                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Nama</td>
                                <td>Username</td>
                                <td>Role</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($dataUser as $row)
                            <tr align="center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->username }}</td>
                                <td>
                                    @foreach ($row->getRoleNames() as $role)
                                    <label for="" class="badge badge-info">{{ $role }}</label>
                                    @endforeach
                                </td>
                                <td>
                                    @if ($row->status=='aktif')
                                    <label class="badge badge-success">Aktif</label>
                                    @else
                                    <label class="badge badge-warning">Suspend</label>
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('user.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <a href="{{ route('user.roles', $row->id) }}" class="btn btn-info btn-sm"><i class="fa fa-user-cog"></i></a>
                                        <a href="{{ route('user.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                        @if( auth()->user()->hasRole('admin') )
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr align="center">
                                <td colspan="12" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection