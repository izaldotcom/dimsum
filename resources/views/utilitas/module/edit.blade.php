  @extends('layouts.master')

  @section('title')
      <title>Edit Modul</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Modul</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('module.index') }}">Modul</a></li>
              <li class="breadcrumb-item active">Edit Modul</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('module.update', $module->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="name" 
                            value="{{ $module->name }}"
                            class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Ikon</label>
                        <input type="text" name="icon" 
                            value="{{ $module->icon }}"
                            class="form-control {{ $errors->has('icon') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('icon') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Url</label>
                        <input type="text" name="url" 
                            value="{{ $module->url }}"
                            class="form-control {{ $errors->has('url') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('url') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <input type="text" name="description" 
                            value="{{ $module->description }}"
                            class="form-control {{ $errors->has('description') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('description') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Bercabang</label>
                        <select name="is_parent" id="is_parent" 
                            required class="form-control {{ $errors->has('is_parent') ? 'is-invalid':'' }}">
                            <option value="0" {{ $module->is_parent == '0' ? 'selected':'' }}>Tidak</option>
                            <option value="1" {{ $module->is_parent == '1' ? 'selected':'' }}>Ya</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('is_parent') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Urutan Modul</label>
                        <input type="number" name="no_order" 
                            value="{{ $module->no_order }}"
                            class="form-control {{ $errors->has('no_order') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('no_order') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-sm">
                            <i class="fa fa-send"></i> Simpan
                        </button>
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                    </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection