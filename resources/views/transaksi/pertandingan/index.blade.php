  @extends('layouts.master')

  @section('title')
      <title>Data Pertandingan</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Pertandingan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">Pertandingan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                @if( auth()->user()->hasRole('admin'))
                <a href="{{ route('pertandingan.create') }}" class="btn btn-primary btn-sm">Tambah Baru</a>
                @endif
                  
                @include ('partials.messages')

                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" id="tabelPertandingan">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Tanggal Pertandingan</td>
                                <td>Waktu Pertandingan</td>
                                <td>Tim Yang Bertanding</td>
                                <td>Skor Akhir</td>
                                <td>Pencetak Gol Home</td>
                                <td>Pencetak Gol Away</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($pertandingan as $row)
                            <tr align="center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ date_format(date_create($row->tgl_pertandingan), "d-m-Y") }}</td>
                                <td>{{ $row->waktu_pertandingan }} menit</td>
                                <td>{{ $row->home->nama }} <b>VS</b> {{ $row->away->nama }}</td>
                                <td>{{ $row->skor_home }} - {{ $row->skor_away }}</td>
                                @php($json_home = json_decode($row->detail_gol_home))
                                @php($json_away = json_decode($row->detail_gol_away))
                                  <td>
                                    @foreach($json_home as $home)
                                      {{ $home->pencetak_gol_home }} {{ $home->menit_gol_home }}'<br>
                                    @endforeach
                                  </td>
                                  <td>
                                    @foreach($json_away as $away)
                                      {{ $away->pencetak_gol_away }} {{ $away->menit_gol_away }}'<br>
                                    @endforeach
                                  </td>
                                <td>
                                    <form action="{{ route('pertandingan.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        @if( auth()->user()->hasRole('admin') )
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr align="center">
                                <td colspan="12" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection