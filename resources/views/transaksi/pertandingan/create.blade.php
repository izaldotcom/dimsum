  @extends('layouts.master')

  @section('title')
      <title>Tambah Pertandingan</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Pertandingan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('pertandingan.index') }}">Pertandingan</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('pertandingan.store') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                      <label for="">Tanggal Pertandingan</label>
                      <input type="text" name="tgl_pertandingan" 
                          class="form-control {{ $errors->has('tgl_pertandingan') ? 'is-invalid':'' }}"
                          id="tgl_pertandingan"
                          >
                      <p class="text-danger">{{ $errors->first('tgl_pertandingan') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Waktu Pertandingan (menit)</label>
                      <input type="text" name="waktu_pertandingan" class="form-control {{ $errors->has('waktu_pertandingan') ? 'is-invalid':'' }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                      <p class="text-danger">{{ $errors->first('waktu_pertandingan') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Home Team</label>
                      <select name="home_team" id="home_team" 
                          required class="form-control {{ $errors->has('home_team') ? 'is-invalid':'' }}">
                          <option value="">Pilih</option>
                          @foreach ($tim as $row)
                              <option value="{{ $row->id }}">{{ ucfirst($row->nama) }}</option>
                          @endforeach
                      </select>
                      <p class="text-danger">{{ $errors->first('home_team') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Away Team</label>
                      <select name="away_team" id="away_team" 
                          required class="form-control {{ $errors->has('away_team') ? 'is-invalid':'' }}">
                          <option value="">Pilih</option>
                          @foreach ($tim as $row)
                              <option value="{{ $row->id }}">{{ ucfirst($row->nama) }}</option>
                          @endforeach
                      </select>
                      <p class="text-danger">{{ $errors->first('away_team') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Skor Home</label>
                      <input type="text" name="skor_home" id="skor_home" class="form-control {{ $errors->has('skor_home') ? 'is-invalid':'' }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                      <p class="text-danger">{{ $errors->first('skor_home') }}</p>
                  </div>
                  <div class="form-group" id="detail_home" style="display: none;">
                    <table class="table table-hover" align="center">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Menit Gol</td>
                                <td>Pencetak Gol</td>
                            </tr>
                        </thead>
                        <tbody id="tbl_skor_home">
                        </tbody>
                    </table>
                  </div>
                  <div class="form-group">
                      <label for="">Skor Away</label>
                      <input type="text" name="skor_away" id="skor_away" class="form-control {{ $errors->has('skor_away') ? 'is-invalid':'' }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                      <p class="text-danger">{{ $errors->first('skor_away') }}</p>
                  </div>
                  <div class="form-group" id="detail_away" style="display: none;">
                    <table class="table table-hover" align="center">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Menit Gol</td>
                                <td>Pencetak Gol</td>
                            </tr>
                        </thead>
                        <tbody id="tbl_skor_away">
                        </tbody>
                    </table>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection