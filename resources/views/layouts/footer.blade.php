  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
  setTimeout(function() {
      $('#notification').fadeOut('slow');
  }, 1500);
  $(document).ready( function () {
    $('#myTable').DataTable();
    $('#tanggal_lahir').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $('#tgl_pertandingan').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
  } );

  //APPEND TABEL PENCETAK SKOR
  $('#skor_home').change(function(){
    var skor_home = $(this).val();
    $('#detail_home').show();
    $('#tbl_skor_home').html('');
    for (var i = 1; i <= skor_home; i++) {
        $('#tbl_skor_home').append('<tr align="center"><td>'+i+'</td><td><input type="number" name="menit_gol_home-'+i+'" id="menit_gol_home-'+i+'"></td><td><input type="text" name="pencetak_gol_home-'+i+'" id="pencetak_gol_home-'+i+'"></td></tr>');
    }
  })
  $('#skor_away').change(function(){
    var skor_away = $(this).val();
    $('#detail_away').show();
    $('#tbl_skor_away').html('');
    for (var i = 1; i <= skor_away; i++) {
        $('#tbl_skor_away').append('<tr align="center"><td>'+i+'</td><td><input type="number" name="menit_gol_away-'+i+'" id="menit_gol_away-'+i+'"></td><td><input type="text" name="pencetak_gol_away-'+i+'" id="pencetak_gol_away-'+i+'"></td></tr>');
    }
  })
  //
  $('#tabelPertandingan').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'print'
      ]
  } );
</script>
</body>
</html>